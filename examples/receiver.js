const RedisQueue = require('../index.js');

// Инициируем очередь
const queue = RedisQueue.initQueue({
  url: null,
  queueName: 'log'
});

let messagesCount = 0;

function getMessages(messages) {
  console.log('Received messages', messages);
  messagesCount += messages.length;
}

//Опрос очереди раз в секунду. getMessages вызывается с массивом сообщений
queue.subscribe(getMessages, 1000);


function getSingleMessage(message) {
  console.log('Received message', message);
  messagesCount += 1;
}

// Подписка на очередь - getMessages вызывается для каждого сообщения
// queue.subscribe(getSingleMessage);

let interval = setInterval(() => {
  if (messagesCount > 20) {
    console.log('Unsubscribing queue');
    clearInterval(interval);
    queue.unsubscribe();
    queue.close()
      .then(() => {
        process.exit();
      });
  }
}, 1000);
