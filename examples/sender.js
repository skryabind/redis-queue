const RedisQueue = require('../index.js');

// Инициируем очередь
const queue = RedisQueue.initQueue({
  url: null,
  queueName: 'log'
});

// Отправляем в очередь 1000 сообщений без ожидания ответа
for (let i = 0; i < 10; i++) {
  queue.sendNowait(`index ${i}`);
}

// Отправляем сообщение и ждем результата
queue.send('Hi!')
  .then(result => {
    console.log(`Receive async result ${result}`);
  });

// Запрашиваем количество сообщений в очереди
queue.getQueueLen()
  .then(res => {
    console.log(`Queue length is ${res}`);
  });

// Закрываем соединение
queue.close()
  .then(() => {
    process.exit();
  });

console.log('Script done');