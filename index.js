'use strict';
const redis = require('redis');

function retryStrategy(options) {
  return 3000;
}

class Queue {
  constructor(url, queueName) {
    this.queueName = queueName;
    this.params = {
      url: url,
      retry_strategy: retryStrategy,
      prefix: 'queue:'
    };
    this.redisClient = redis.createClient(this.params);
  }

  /**
   * Возвращает статус подключения к Redis
   * @return {boolean}
   */
  getConnected() {
    return this.redisClient.connected;
  }

  /**
   * Отправляет сообщение в очередь.
   * @param message
   * @return {Promise<number>} Количество сообщений в очереди
   */
  send(message) {
    return new Promise((resolve, reject) => {
      this.redisClient.rpush(this.queueName, message, (err, reply) => {
        if (err) {
          reject(err);
        } else {
          resolve(reply);
        }
      });
    });
  }

  /**
   * Отправляет сообщение без ожидания. Если есть проблемы с подключением к Redis, сообщение ставится в очередь
   * и будет отправлено при появлении подключения.
   * @param message
   * @return {boolean} Если в текущий момент подключения к Redis нет, возвращает false, иначе true
   */
  sendNowait(message) {
    return this.redisClient.rpush(this.queueName, message);
  }

  /**
   * Возвращает длину очереди
   * @return {Promise<number>}
   */
  getQueueLen() {
    return new Promise((resolve, reject) => {
      this.redisClient.llen(this.queueName, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  subscribe(fn, pollPeriod) {
    this.subscriberFn = fn;
    if (pollPeriod) { // запускаем периодический опрос очереди
      setTimeout(() => this.pollPeriod(pollPeriod), 0);
    } else { // Опрашиваем очередь постоянно
      setTimeout(async () => this.pollForever(), 0);
    }
  }

  /**
   * Запускает периодический опрос. Вызывает функцию subscribeFn для массива сообещний, которые пришли в очередь
   * с момента последнего опроса.
   * @param period Время в мс для опроса очереди.
   */
  pollPeriod(period) {
    this.subscribeInterval = setInterval(async () => {
      let messages = [];
      let loop = true;
      while (this.subscriberFn && loop) {
        try {
          const message = await this.fetchMessage(0);
          if (message) {
            messages.push(message);
          } else {
            loop = false;
          }
        } catch (e) {
          loop = false;
        }
      }
      if (this.subscriberFn && messages.length > 0) {
        this.subscriberFn(messages);
      }
    }, period);
  }

  /**
   * Запускает опрос очереди, для каждого сообщения вызывает функцию subscribeFn
   */
  async pollForever() {
    while (this.subscriberFn) {
      try {
        let result = await this.fetchMessage(1);
        if (this.subscriberFn && result) {
          this.subscriberFn(result[1]);
        }
      } catch (e) {
        console.log('Poll error ' + e.toString() + ' waiting 30 seconds');
        const fn = this.subscriberFn;
        this.subscriberFn = null;
        let self = this;
        setTimeout(async () => {
          self.subscriberFn = fn;
          await self.pollForever();
        }, 30000);
      }
    }
  }

  /**
   * Возвращает одно сообщение из очереди
   * @param waitTime Время ожиданя сообщения
   * @return {Promise<string>} Сообщение
   */
  fetchMessage(waitTime) {
    return new Promise((resolve, reject) => {
      if (waitTime) { // Ожидаем сообщения
        this.redisClient.blpop(this.queueName, waitTime, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      } else { // Не ждем сообщения, возвращаем сразу что есть.
        this.redisClient.lpop(this.queueName, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      }
    });
  }

  unsubscribe() {
    if (this.subscribeInterval) {
      clearInterval(this.subscribeInterval);
    }
    this.subscriberFn = null;
  }

  /**
   * Закрывает подключение к Redis. Если есть сообщения в очереди, они отправляются перед закрытием.
   * @return {Promise<string>}
   */
  close() {
    return new Promise((resolve, reject) => {
      this.redisClient.quit((err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(result);
        }
      });
    });
  }
}

/**
 * Инициирует объект очереди
 * @param params
 * @return {Queue}
 */
function initQueue(params) {
  return new Queue(params.url, params.queueName);
}

exports.initQueue = initQueue;
