# redis-queue

Модуль для создания простой очереди на основне списков Redis db.


## Отправка сообщений в очередь
Есть 2 варианта отправки сообщения: с ожиданием и без.
Первый вариант возвращает promise, который резольвится в случае успешной вставки сообщения в очередь. Promise возвращает количество сообщений в очереди

Второй вариант, без ожидания, просто возвращает true или false и с ставит сообщение в очередь на отправку.

Важно, что если в данный момент коннекта к Redis нет, то все сообщения ставятся в очередь и будут отправлены, когда соединение вновь появится.

```javascript
const RedisQueue = require('redis-queue');

// Инициируем очередь
const queue = RedisQueue.initQueue({
  url: 'redis://user:password@host:port',
  queueName: 'log'
});

// Отправляем сообщение и ждем результата
queue.send('Hi!')
  .then(result => {
    console.log(`Receive async result ${result}`);
  });

// Отправляем в очередь 1000 сообщений без ожидания ответа
for (let i = 0; i < 10; i++) {
  queue.sendNowait(`index ${i}`);
}

// Запрашиваем количество сообщений в очереди
queue.getQueueLen()
  .then(res => {
    console.log(`Queue length is ${res}`);
  });

// Закрываем соединение
queue.close()
  .then(() => {
    process.exit();
  });

```

## Получение сообщения из очереди
Есть 2 варианта подписки на сообщения:
1. На каждое сообщение будет вызвана колбек-функция.
2. Очередь будет опрашиваться периодически и колбек-функция будет вызываться для пачки сообщений, которые пришли за период опроса.

После получения сообщения они сразу удаляются из очереди, поэтому в случае какой-то ошибки рекомендуется вручную вставлять сообщения обратно в очередь.

```javascript
const RedisQueue = require('redis-queue');

// Инициируем очередь
const queue = RedisQueue.initQueue({
  url: 'redis://user:password@host:port',
  queueName: 'log'
});

let messagesCount = 0;

function getMessages(messages) {
  console.log('Received messages', messages);
  messagesCount += messages.length;
}

//Опрос очереди раз в секунду. getMessages вызывается с массивом сообщений
queue.subscribe(getMessages, 1000);


function getSingleMessage(message) {
  console.log('Received message', message);
  messagesCount += 1;
}

// Подписка на очередь - getMessages вызывается для каждого сообщения
// queue.subscribe(getSingleMessage);

let interval = setInterval(() => {
  if (messagesCount > 20) {
    console.log('Unsubscribing queue');
    clearInterval(interval);
    queue.unsubscribe();
    queue.close()
      .then(() => {
        process.exit();
      });
  }
}, 1000);
```